<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Welcome to GeekAndProud.co.uk - Home to Geeks, Nerds, Dorks and Other Social Outcasts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="verify-v1" content="CdL/gPZqKYtZMDafsDZax76s0OdYAPmmBH1oyp+70uM=" >
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<link href="styles.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="menu" style="position:absolute; left:0px; top:0px; width:150px; height:500px; z-index:1"> 
  <table width="95%" height="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="B8B789">
    <tr> 
      <td><ul>
          <li class="MenuText">Home</li>
          <li class="MenuText"><a href="docs/about.htm"class="MenuText">About</a></li>
          <li class="MenuText"><a href="docs/stuff.htm"class="MenuText">Bits &amp; Bobs</a></li>
          <li class="MenuText"><a href="../docs/shop.php"class="MenuText">Shop</a></li>
          <li class="MenuText"><a href="docs/downloads.htm"class="MenuText">Downloads</a></li>
          <li class="MenuText"><a href="docs/cv.htm"class="MenuText">Resum&eacute;</a></li>
          <li class="MenuText"><a href="docs/links.html" class="MenuText">Links</a></li>
          <li class="MenuText"><a href="docs/contacts.htm" class="MenuText">Contacts</a></li>
        </ul>
        <p align="center"><a href="feeds/feed_news.php"><img src="pics/feedicons/07.png" alt="News RSS Feed" width="12" height="12" border="0"></a></p></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
<div id="TopBar" style="position:absolute; left:150px; top:0px; width:500px; height:100px; z-index:2"><img src="pics/newtitlebig.jpg" alt="Welcome to GeekAndProud.co.uk now incorporating JBI" width="500" height="100"></div>
<div id="Main" style="position:absolute; left:150px; top:100px; width:500px; height:400px; z-index:3; overflow: auto;">
  <p class="basic">
    <?php
// Opens the connection to the database
require('./admin/db_connect.php'); 

// Executes the SQL query
$sql='SELECT * FROM gap_news ORDER BY NewsDate DESC LIMIT 10';
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

// Outputing the results
echo "  <table width='100%' border='0' cellpadding='0' cellspacing='0' bordercolor='#000000'>";
while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    echo "\t<tr>\n";
    echo "\t\t<td class='NewsDate' width='40%'>$line[NewsDate]</td><td class='NewsDate' width='60%' align='center'>$line[NewsTitle]</td>\n";
    echo "\t</tr>\n";
	echo "\t<tr><td colspan='2' class='NewsBody'><p>$line[NewsItem]</p></td>\n";
	echo "\t</tr>\n";
	echo "\t<tr>\n";
	echo "<td><br></td>";
	echo "\t</tr>\n";
	echo "\t<tr>\n";
// Bit for Comments
$query = "SELECT COUNT(*) FROM gap_news_comments WHERE NewsID = $line[NewsID]";
$result2 = mysql_query($query) or die('Query failed: ' . mysql_error());
$count = mysql_fetch_row($result2);

	echo "<td colspan='2' class='NewsDate'  align='center'><a href='docs/news_details.php?id=$line[NewsID]#comments'>".$count[0]." Comments</a> | <a href='docs/comment.php?nid=$line[NewsID]'>Add Comment</a></td>\n";
	echo "<tr><td colspan='2' class='NewsBody'><hr align='center' width='50%'><br><br></td>\n";
	echo "</tr>\n";
	}
	echo "</table>\n";

// Free resultset
mysql_free_result($result);

// Closing connection
mysql_close($link);

?>
  <p align="center" class="ContactLinks"><a href="docs/news_arch.php">News Archive</a>  
  <p align="center" class="ContactLinks"><a href="feeds/feed_news.php">Get The News Feed</a>
    <a href="feeds/feed_news.php"><img src="pics/feedicons/07.png" alt="RSS Feed" width="12" height="12" border="0"></a></div>
<div id="Quote" style="position:absolute; left:0px; top:550px; width:650px; height:30px; z-index:4" class="quotes"> 
  <div align="center"class="quotes"> 
    <script src="quoter.js" type="text/javascript" ></script>
  </div>
</div>
<div id="SpareMenu" style="position:absolute; left:0px; top:500px; width:650px; height:50px; z-index:4"><!-- SiteSearch Google -->
<form method="get" action="http://www.geekandproud.co.uk/docs/search_results.htm" target="_top">
    <table border="0" align="center" cellpadding="0" cellspacing="0" class="basic">
      <tr><td nowrap="nowrap" valign="top" align="left" height="32">
<a href="http://www.google.com/">
<img src="http://www.google.com/logos/Logo_25wht.gif" border="0" alt="Google" align="middle"></img></a>
</td>
<td nowrap="nowrap">
<input type="hidden" name="domains" value="www.geekandproud.co.uk"></input>
<label for="sbi" style="display: none">Search:</label>
<input type="text" name="q" size="15" maxlength="255" value="" id="sbi"></input>
<input type="submit" name="sa" value="Search" id="sbb"></input>
          <input type="radio" name="sitesearch" value="" id="ss0"></input><label for="ss0" title="Search the Web"><font size="-1" color="#000000">Web</font></label><input type="radio" name="sitesearch" value="www.geekandproud.co.uk" checked id="ss1"></input>
<label for="ss1" title="Search www.geekandproud.co.uk"><font size="-1" color="#000000">Geek And Proud</font></label></td></tr>
<tr>
<td>&nbsp;</td>
<td nowrap="nowrap">
<table>
<tr>
              <td>  
                </td>
<td>
</td>
</tr>
</table>
<input type="hidden" name="client" value="pub-8710215801764524"></input>
<input type="hidden" name="forid" value="1"></input>
<input type="hidden" name="channel" value="4975115109"></input>
<input type="hidden" name="ie" value="ISO-8859-1"></input>
<input type="hidden" name="oe" value="ISO-8859-1"></input>
<input type="hidden" name="cof" value="GALT:#666666;GL:1;DIV:#B8B789;VLC:B8B789;AH:center;BGC:E8E8C6;LBGC:E8E8C6;ALC:B8B789;LC:B8B789;T:000000;GFNT:666666;GIMP:666666;FORID:11"></input>
<input type="hidden" name="hl" value="en"></input>
</td></tr></table>
</form>
<!-- SiteSearch Google --></div>
<div id="content" style="position:absolute; left:650px; top:0px; width:140px; height:580px; z-index:5">
  <table width="100%" height="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="left" valign="middle" class="basic"> 
        <p class="basic">
        <script type="text/javascript" src="http://www.google.com/reader/ui/publisher-en.js"></script>
<script type="text/javascript" src="http://www.google.com/reader/public/javascript/user/02087999808058249104/state/com.google/broadcast?n=5&callback=GRC_p(%7Bc%3A%22-%22%2Ct%3A%22What%20I%20am%20Reading%22%2Cs%3A%22false%22%2Cn%3A%22true%22%2Cb%3A%22false%22%7D)%3Bnew%20GRC"></script>
        
      </p></td>
    </tr>
    <tr>
    <td></td>
    </tr>
    <tr>
    <td align="center" valign="middle"><script type="text/javascript" charset="UTF-8" src="tinc?key=hmsYS1IG"></script></td>
    </tr>
  </table>
</div>
<p class="basic">&nbsp;</p>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-12795818-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GeekAndProud.co.uk - Stuff - Photography</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<link href="../../styles.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="menu" style="position:absolute; left:0px; top:0px; width:150px; height:500px; z-index:1"> 
  <div id="content" style="position:absolute; left:650px; top:0px; width:140px; height:580px; z-index:5"> 
    <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr> 
        <td align="center" valign="bottom"><script type="text/javascript"><!--
google_ad_client = "pub-8710215801764524";
google_ad_width = 120;
google_ad_height = 600;
google_ad_format = "120x600_as";
google_ad_type = "text_image";
//2007-05-10: test
google_ad_channel = "7796931441";
google_color_border = "B8B789";
google_color_bg = "E8E8C6";
google_color_link = "B8B789";
google_color_text = "000000";
google_color_url = "666666";
//-->
</script> <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
      </tr>
    </table>
  </div>
  <table width="95%" height="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="B8B789">
    <tr> 
      <td><ul>
          <li class="MenuText"><a href="../../../index.php"class="MenuText">Home</a></li>
          <li class="MenuText"><a href="../about.htm"class="MenuText">About</a></li>
          <li class="MenuText"><a href="../stuff.htm"class="MenuText">Bits &amp; Bobs</a></li>
          <li class="MenuText"><a href="../../../docs/shop.php"class="MenuText">Shop</a></li>
          <li class="MenuText"><a href="../downloads.htm"class="MenuText">Downloads</a></li>
          <li class="MenuText"><a href="../cv.htm"class="MenuText">Resum&eacute;</a></li>
          <li class="MenuText"><a href="../links.html" class="MenuText">Links</a></li>
          <li class="MenuText"><a href="../contacts.htm"class="MenuText">Contacts</a></li>
        </ul>
        <p align="center"><a href="../feeds/feed_news.php"><img src="../../pics/feedicons/07.png" alt="News RSS Feed" width="12" height="12" border="0"></a></p></td>
    </tr>
  </table>
</div>
<div id="TopBar" style="position:absolute; left:150px; top:0px; width:500px; height:100px; z-index:2"><img src="../../pics/stuff/photo.jpg" alt="Photography" width="500" height="100"></div>
<div id="Main" style="position:absolute; left:150px; top:100px; width:500px; height:400px; z-index:3; overflow: auto;" class="basic">
  <p class="basic">Digital photography is a large part of my life. I enjoy the ability of being 
    able to take as many pictures as you like until you get one you like. I also 
    like the instant results, it makes sharing images a lot easier. Some might 
    say this takes the skill out of photography I, however, disagree. You are 
    more likely to attempt adventurous shots if you aren't counting frames.</p>
  <p class="basic">Here I have some of my better images for you to look through. Until I can write my own gallery software I am using Google's <a href="http://picasa.google.com" target="_blank">Picasa</a> to host my images. The galleries will open in a new window.</p>
   <?php
// Opens the connection to the database
require('../../admin/db_connect.php'); 

// Executes the SQL query
$sql = 'SELECT gal_name, gal_url, gal_description '
        . ' FROM gap_galleries as g JOIN gap_galtypes as gt ON (g.gal_type=gt.typeID)'
        . ' WHERE gt.type_name="Photography" ';
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

// Outputing the results
while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
echo "<p align='center' >";
echo "<a href='$line[gal_url]' target=_blank class='ContactLinks'>$line[gal_name]</a><br>";
echo "$line[gal_description]";
echo "</p>";
}
// Free resultset
mysql_free_result($result);

// Closing connection
mysql_close($link);

?>
</p>
</div>
<div id="Quote" style="position:absolute; left:0px; top:550px; width:650px; height:30px; z-index:4" class="quotes"> 
  <div align="center"> 
    <script src="../../quoter.js" type="text/javascript" class="quotes"></script>
  </div>
</div>
<div id="SpareMenu" style="position:absolute; left:0px; top:500px; width:650px; height:50px; z-index:4"></div>
<p class="basic">&nbsp;</p>
</body>
</html>

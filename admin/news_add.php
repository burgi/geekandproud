<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GeekAndProud.co.uk - Admin - News - Add</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="verify-v1" content="CdL/gPZqKYtZMDafsDZax76s0OdYAPmmBH1oyp+70uM=" />
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="menu" style="position:absolute; left:0px; top:0px; width:150px; height:500px; z-index:1"> 
  <table width="95%" height="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="B8B789">
    <tr> 
      <td><ul>
          <li class="MenuText">Add  News</li>
          <li class="MenuText">Edit News</li>
        </ul>
        <p align="center"><a href="feeds/feed_news.php"><img src="pics/feedicons/07.png" alt="News RSS Feed" width="12" height="12" border="0"></a></p></td>
    </tr>
  </table>
</div>
<div id="TopBar" style="position:absolute; left:150px; top:0px; width:500px; height:100px; z-index:2"><img src="../pics/newtitlebig.jpg" alt="GeekAndProud.co.uk Admin Control Panel" width="500" height="100"></div>
<div  id="Main" style="position:absolute; left:150px; top:100px; width:500px; height:400px; z-index:3; overflow: auto;">
  <p class="basic">Adding a news item here will make it viewable on the main page.</p>
  <form action="db_insertnews.php" method="post" name="frm_add" id="frm_add">
    <table width="100%" border="0">
      <tr>
        <td width="25%" class="basic">News Title</td>
        <td width="75%"><input name="txt_title" type="text" id="txt_title" size="60" maxlength="200"></td>
      </tr>
      <tr>
        <td width="25%" class="basic">News Item</td>
        <td width="75%"><textarea name="txt_news" id="txt_news" cols="60" rows="8"></textarea></td>
      </tr>
      <tr>
        <td colspan="2"><div align="center">
          <input type="submit" name="btn_submit" id="btn_submit" value="Submit">
          <input type="reset" name="btn_reset" id="btn_reset" value="Reset">
        </div></td>
      </tr>
    </table>
    <label></label>
  </form>
  <p class="basic">&nbsp;</p>
</div>
<div id="Quote" style="position:absolute; left:0px; top:550px; width:650px; height:30px; z-index:4" class="quotes"> 
  <div align="center"> 
    <script src="quoter.js" type="text/javascript" class="quotes"></script>
  </div>
</div>
<div id="SpareMenu" style="position:absolute; left:0px; top:500px; width:650px; height:50px; z-index:4"><!-- SiteSearch Google -->
</div>
<div id="content" style="position:absolute; left:650px; top:0px; width:140px; height:580px; z-index:5">
  <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="bottom"> 
        <p>
      </p></td>
    </tr>
  </table>
</div>
<p class="basic">&nbsp;</p>
</body>
</html>

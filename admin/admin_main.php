<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Azia's Bolthole - Admin Screen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body bgcolor="#000000" link="#666666" vlink="#CCCCCC" alink="#000066">
<div id="sideLogo" style="position:absolute; left:50px; top:0px; width:95px; height:600px; z-index:2"><a href="../index.php"><img src="../pics/sidelogo.jpg" alt="Azia's Bolthole" width="95" height="600" border="0"></a></div>
<div id="links" style="position:absolute; left:146px; top:500px; width:680px; height:100px; z-index:3;">
  <table width="100%" border="0" align="center">
    <tr>
      <td width="20%" class="menutext"><a href="admin_newsadd.php">Add News</a></td>
      <td width="20%" class="menutext"><a href="admin_editnews.php">Edit News</a></td>
      <td width="20%" class="menutext"><a href="admin_newburial.php">New Burial</a></td>
      <td width="20%" class="menutext"><a href="admin_searchburial.php">Update Burial</a></td>
      <td width="20%" class="menutext"><a href="admin_delcomments.php">Delete Comments</a></td>
    </tr>
    <tr>
      <td width="20%">&nbsp;</td>
      <td width="20%" class="menutext">&nbsp;</td>
      <td width="20%" class="menutext">&nbsp;</td>
      <td width="20%" class="menutext">&nbsp;</td>
      <td width="20%">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5" align="center" class="basic"><div id="toolbar">
        <a class="menu" href="index.php" title="Return to the home page">Home</a>
        <a class="menu" href="pages/page1.php" title="Visit Menu Item 1">About</a>
        <a class="menu" href="pages/page2.php" title="Visit Menu Item 1">Archeology</a>
        <a class="menu" href="pages/page3.php" title="Visit Menu Item 1">Things</a>
        <a class="menu" href="pages/page4.php" title="Visit Menu Item 1">Contact</a>
        </div></td>
    </tr>
  </table>
</div>
<div id="title" style="position:absolute; left:145px; top:0px; width:680px; height:95px; z-index:4">
  <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="middle" class="TitleText"><img src="../pics/titles/welcome.jpg" width="500" height="90" alt="Welcome To Azia's Bolthole - Azia's Hidey Hole and Blog"></td>
    </tr>
  </table>
</div>
<div class="basic" id="main" style="position:absolute; left:147px; top:95px; width:680px; height:404px; z-index:5; overflow: auto;">
  <p>Welcome to the admin section of Azia's Bolthole.</p>
  <p align="center" class="TableTitle">Website Stats</p>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" class="TableTitle">News</td>
          </tr>
        <tr>
          <td width="50%" class="basic">Num of News Items</td>
          <td width="50%" align="center" class="basic"><a href="../newdocs/archive.php"><?php 
	  // Opens the connection to the database
		require('dbconnect.php');
		// Executes the SQL query
		$q1="SELECT COUNT(*) FROM eve_news";
		$q2="SELECT COUNT(*) FROM eve_comments";
		$r1 = mysql_query($q1) or die('Query failed: ' . mysql_error());
		$r2 = mysql_query($q2) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$x= mysql_fetch_array($r1, MYSQL_ASSOC);
		$y= mysql_fetch_array($r2, MYSQL_ASSOC);
		foreach ($x as $news);
		foreach ($y as $comments);
		echo $news;
	  ?>
          </a></td>
        </tr>
        <tr>
          <td width="50%" class="basic">Num Of Comments</td>
          <td width="50%" align="center" class="basic"><?php echo $comments; ?></td>
        </tr>
        <tr>
          <td width="50%" class="basic">Latest Comment</td>
          <td width="50%" align="center" class="basic"><?php echo round($comments/$news,2); ?></td>
        </tr>
      </table></td>
      <td><table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" class="TableTitle">Cemetery</td>
          </tr>
        <tr>
          <td width="50%" class="basic">Num Burials</td>
          <td align="center" class="basic"><?php 
	  
		// Executes the SQL query
		$q1="SELECT COUNT(*) FROM eve_corpses";
		$q2="SELECT COUNT(*) FROM eve_burials";
		$r1 = mysql_query($q1) or die('Query failed: ' . mysql_error());
		$r2 = mysql_query($q2) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$x= mysql_fetch_array($r1, MYSQL_ASSOC);
		$y= mysql_fetch_array($r2, MYSQL_ASSOC);
		foreach ($x as $corpse);
		foreach ($y as $burial);
		echo $burial;
	  ?></td>
        </tr>
        <tr>
          <td width="50%" class="basic">Num Corpses</td>
          <td align="center" class="basic"><?php echo $corpse; ?></td>
        </tr>
        <tr>
          <td width="50%" class="basic">Corpse/Burial Ratio</td>
          <td align="center" class="basic">&nbsp;</td>
        </tr>
      </table></td>
      <td><table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" class="TableTitle">Corpse Statuses</td>
          </tr>
        <tr>
          <td width="50%" class="basic">Stolen</td>
          <td class="basic"><?php 
  $query="SELECT COUNT(s.StatusDesc)"
        . " FROM eve_corpses AS c LEFT JOIN eve_corpsestatus AS s ON (c.CStatusID=s.CStatusID)"
		."WHERE s.StatusDesc ='Stolen'"
        . " GROUP BY s.StatusDesc";
			$r3 = mysql_query($query) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$z= mysql_fetch_array($r3, MYSQL_ASSOC);
		foreach ($z as $grid);
		if ($grid=="")
		echo "0";
		else
  echo $grid;
  ?></td>
        </tr>
        <tr>
          <td width="50%" class="basic">Missing</td>
          <td class="basic"><?php 
  $query="SELECT COUNT(s.StatusDesc)"
        . " FROM eve_corpses AS c LEFT JOIN eve_corpsestatus AS s ON (c.CStatusID=s.CStatusID)"
		."WHERE s.StatusDesc ='Missing'"
        . " GROUP BY s.StatusDesc";
			$r3 = mysql_query($query) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$z= mysql_fetch_array($r3, MYSQL_ASSOC);
		foreach ($z as $grid);
  echo $grid;
  mysql_free_result($r3);
  ?></td>
        </tr>
        <tr>
          <td width="50%" class="basic">To Bury</td>
          <td class="basic"><center><a href="admin_tobury.php"><?php 
		  
  $query2="SELECT COUNT(s.StatusDesc)"
        . " FROM eve_corpses AS c LEFT JOIN eve_corpsestatus AS s ON (c.CStatusID=s.CStatusID)"
		."WHERE s.StatusDesc ='To Bury'"
        . " GROUP BY s.StatusDesc";
			$r4 = mysql_query($query2) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$a= mysql_fetch_array($r4, MYSQL_ASSOC);
		foreach ($a as $g2);
		if ($g2=="")
		echo "0";
		else
  		echo $g2;
		mysql_free_result($r4);
  ?></a></center></td>
        </tr>
        <tr>
          <td class="basic">Buried</td>
          <td class="basic"><center><?php 
  $query="SELECT COUNT(s.StatusDesc)"
        . " FROM eve_corpses AS c LEFT JOIN eve_corpsestatus AS s ON (c.CStatusID=s.CStatusID)"
		."WHERE s.StatusDesc ='Buried'"
        . " GROUP BY s.StatusDesc";
			$r3 = mysql_query($query) or die('Query failed: ' . mysql_error());
		// Outputs the results in HTML
		$z= mysql_fetch_array($r3, MYSQL_ASSOC);
		foreach ($z as $grid);
  echo $grid;
  ?></center>
            </td>
        </tr>
      </table></td>
    </tr>
  </table>
  <p class="menutext"><a href="http://admin.oneandone.co.uk" target="_blank">1&amp;1 Admin</a></p>
  <p class="menutext"><a href="http://adsense.google.com" target="_blank">Google Adsense</a></p>
</div>
<div id="adverts" style="position:absolute; left:830px; top:0px; width:125px; height:600px; z-index:6"></div>
</body>
</html>
